Find
1. Bookmark the Neo4j wiki.
https://neo4j.com/docs/
2. Bookmark the Gremlin steps from the wiki or API.
https://tinkerpop.apache.org/docs/3.1.0-incubating/#neo4j-gremlin
3. Find two other Neo4j shells (such as the Cypher shell in the admin console).
https://tinkerpop.apache.org/
https://neo4j.com/docs/operations-manual/current/tools/cypher-shell/


Do
1. Query all node names with another shell (such as the Cypher query language).
MATCH (n) RETURN distinct labels(n), count(*)
2. Delete all the nodes and edges in your database.
g.V().drop()
g.E().drop()
3. Create a new graph that represents your family
--
