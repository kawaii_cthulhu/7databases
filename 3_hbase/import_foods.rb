#---
# Excerpted from "Seven Databases in Seven Weeks",
# published by The Pragmatic Bookshelf.
# Copyrights apply to this code. It may not be used to create training material, 
# courses, books, articles, and the like. Contact us if you are in doubt.
# We make no guarantees that this code is fit for any purpose. 
# Visit http://www.pragmaticprogrammer.com/titles/rwdata for more book information.
#---

require 'time'

import 'org.apache.hadoop.hbase.client.Connection'
import 'org.apache.hadoop.hbase.client.ConnectionFactory'
import 'org.apache.hadoop.hbase.client.Table'
import 'org.apache.hadoop.hbase.client.Put'
import 'javax.xml.stream.XMLStreamConstants'

def jbytes( *args )
  args.map { |arg| arg.to_s.to_java_bytes }
end

factory = javax.xml.stream.XMLInputFactory.newInstance
reader = factory.createXMLStreamReader(java.lang.System.in)

document = nil # (1)
buffer = nil
count = 0

conn = ConnectionFactory.createConnection()
table = conn.getBufferedMutator(TableName.valueOf("foods"))
table.disableWriteBufferPeriodicFlush() # (2)

while reader.has_next
  type = reader.next
  
  if type == XMLStreamConstants::START_ELEMENT # (3)
  
    case reader.local_name
    when 'Food_Display_Row' then document = {}
    else buffer = []
    end
    
  elsif type == XMLStreamConstants::CHARACTERS # (4)
    
    buffer << reader.text unless buffer.nil?
    
  elsif type == XMLStreamConstants::END_ELEMENT # (5)
    
    case reader.local_name
    when 'Food_Display_Row' then
      key = document['Display_Name'].to_java_bytes
      ts = ( document['Food_Code'] ).to_i
      document.delete('Display_Name')
      document.delete('Food_Code')
      
      p = Put.new( key, ts )
      document.each do |key, value|
        p.addColumn( *jbytes('info', key, value ) )
      end
      table.mutate( p )
      
      count += 1
      table.flush() if count % 10 == 0
      if count % 500 == 0
        puts "#{count} records inserted (#{key})"
      end
    else
      document[reader.local_name] = buffer.join
    end
  end
end

table.close()
conn.close()
exit



