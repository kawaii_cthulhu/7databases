import pymongo


def connect():
    client = pymongo.MongoClient('localhost', 9332)
    return client


def populate(db):
    db.numbers.insert_many([{'int': i, 'str': str(i)} for i in range(1001)])


def create_index(db):
    result = db.numbers.create_index([('int', pymongo.ASCENDING)],
                                     unique = True)
    print(result)


if __name__ == '__main__':
    client = connect()
    db = client.test
    populate(db)
    create_index(db)
    client.close()