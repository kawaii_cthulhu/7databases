CREATE RULE update_holidays AS ON UPDATE TO holidays DO INSTEAD
  UPDATE events
  SET title = NEW.name,
    starts = NEW.date,
    colors = NEW.colors
  WHERE title = OLD.name;

CREATE RULE insert_holidays AS ON INSERT TO holidays DO INSTEAD
  INSERT INTO events (title, starts, ends, venue_id, colors) VALUES
  (NEW.name, NEW.date, NEW.date, null, NEW.colors);